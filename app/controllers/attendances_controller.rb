class AttendancesController < ApplicationController
  before_action :authenticate_user!

  def index
    @attendances = current_user.attendances
    @attendance = current_attendance || Attendance.new
  end

  def create
    @attendance = current_user.attendances.create(clock_in: Time.zone.now)
  end

  def update
    Attendance.find(params[:id]).clockout
    @attendance = Attendance.new
  end
end
