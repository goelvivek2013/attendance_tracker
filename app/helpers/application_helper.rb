module ApplicationHelper

  def current_attendance
    current_user.attendances.where.not(clock_in: nil).find_by(created_at: Date.today..Time.zone.now, clock_out: nil)
  end
end
