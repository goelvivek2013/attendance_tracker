class Attendance < ApplicationRecord
  belongs_to :user

  def clockout
    update(clock_out: Time.zone.now, duration: (Time.zone.now - clock_in))
  end
end
