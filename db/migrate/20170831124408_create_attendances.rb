class CreateAttendances < ActiveRecord::Migration[5.0]
  def change
    create_table :attendances do |t|
      t.references :user, foreign_key: true
      t.datetime :clock_in
      t.datetime :clock_out
      t.integer :duration

      t.timestamps
    end
  end
end
